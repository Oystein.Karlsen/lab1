package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import javax.lang.model.util.ElementScanner14;

public class RockPaperScissors {

    public static void main(String[] args) {
        /*
         * The code here does two things:
         * It first creates a new RockPaperScissors -object with the
         * code `new RockPaperScissors()`. Then it calls the `run()`
         * method on the newly created object.
         */
        new RockPaperScissors().run();
    }

    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");

    public void run() {
        while (true) {

            System.out.println("Let's play round " + roundCounter);
            String choice = readInput("Your choice (Rock/Paper/Scissors)?");

            
            if (!choice.equals("rock") && !choice.equals("paper") && !choice.equals("scissors")) {
                System.out.println("I do not understand " + choice + ". Could you try again?");
            } else {

                String compChoice = getRandomAnswer();
                String result = "none";

                if (choice.equals(compChoice)) {
                    result = "It's a tie!";
                } else if ((choice.equals("rock") && compChoice.equals("scissors"))
                        || (choice.equals("scissors") && compChoice.equals("paper"))
                        || (choice.equals("paper") && compChoice.equals("rock"))) {

                    result = "Human wins!";
                    humanScore++;
                } else {
                    result = "Computer wins!";
                    computerScore++;
                }
                roundCounter++;
                System.out.println("Human chose " + choice + ", computer chose " + compChoice + ". " + result);
                System.out.println("Score: human " + humanScore + ", computer " + computerScore);
                if (readInput("Do you wish to continue playing? (y/n)?").equals("n")) {
                    break;
                }

            }

        }
        System.out.println("Bye bye :)");
    }

    /**
     * Reads input from console with given prompt
     * 
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

    public String getRandomAnswer() {
        int r = (int) (Math.random() * rpsChoices.size());
        String compChoice = rpsChoices.get(r);
        return compChoice;
    }

}
